<?php

declare(strict_types=1);

namespace App\Entity;

use Carbon\Carbon;
use DateTime;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="invoices")
 */
class Invoice
{
    /**
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $invoiceId;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $amount;

    /**
     * @var string
     *
     * @ORM\Column(type="string")
     */
    protected $price;

    /**
     * @var DateTime
     *
     * @ORM\Column(type="date")
     */
    protected $expiredAt;

    /**
     * Invoice constructor.
     *
     * @param string            $invoiceId
     * @param string            $amount
     * @param DateTimeInterface $expiredAt
     */
    public function __construct(
        string $invoiceId,
        string $amount,
        DateTimeInterface $expiredAt
    ) {
        $this->invoiceId = $invoiceId;
        $this->amount = $amount;
        $this->expiredAt = $expiredAt;
        $this->price = $this->calculatePrice();
    }

    /**
     * @return string
     */
    public function getInvoiceId(): string
    {
        return $this->invoiceId;
    }

    /**
     * @return string
     */
    public function getAmount(): string
    {
        return $this->amount;
    }

    /**
     * @return string
     */
    public function getPrice(): string
    {
        return $this->price;
    }

    /**
     * @return DateTime
     */
    public function getExpiredAt(): DateTime
    {
        return $this->expiredAt;
    }

    /**
     * @return string
     */
    protected function calculatePrice(): string
    {
        $coefficient = Carbon::now()->subDays(30)->gt($this->expiredAt)
            ? 0.5
            : 0.3;

        return (string)($this->amount * $coefficient);
    }
}
