<?php

declare(strict_types=1);

namespace App\Service;

use App\Entity\Invoice;
use App\Repository\InvoiceRepository;
use Carbon\Carbon;
use Doctrine\ORM\EntityManagerInterface;
use Exception;
use League\Csv\Reader;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class InvoiceService
{
    /** @var EntityManagerInterface */
    private $entityManager;

    /** @var InvoiceRepository */
    private $invoiceRepository;

    /**
     * @param EntityManagerInterface $entityManager
     * @param InvoiceRepository      $invoiceRepository
     */
    public function __construct(
        EntityManagerInterface $entityManager,
        InvoiceRepository $invoiceRepository
    ) {
        $this->entityManager = $entityManager;
        $this->invoiceRepository = $invoiceRepository;
    }

    /**
     * @return array
     */
    public function getInvoices(): array
    {
        return $this->invoiceRepository->findAll();
    }

    /**
     * @param UploadedFile $file
     *
     * @return array
     * @throws Exception
     */
    public function createInvoices(UploadedFile $file): array
    {
        $reader = Reader::createFromPath($file->getPathname());

        foreach ($reader->fetchAll() as $row) {
            $invoice = (new Invoice($row[0], $row[1], Carbon::parse($row[2])));
            $this->entityManager->persist($invoice);
        }

        $this->entityManager->flush();

        return [];
    }
}
