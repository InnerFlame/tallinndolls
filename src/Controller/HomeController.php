<?php

declare(strict_types=1);

namespace App\Controller;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /** @var Client */
    private $client;

    /**
     * @param Client $client
     */
    public function __construct(Client $client)
    {
        $this->client = $client;
    }

    /**
     * @Route("/", methods={"GET"})
     *
     * @return Response
     *
     * @throws GuzzleException
     */
    public function index(): Response
    {
        $response = $this->client->get('webserver/api/invoice');

        return $this->render('home/index.html.twig', json_decode($response->getBody()->getContents(), true));
    }
}