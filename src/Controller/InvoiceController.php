<?php

declare(strict_types=1);

namespace App\Controller;

use App\Service\InvoiceService;
use Exception;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Throwable;

/**
 * @Route("/api", name="invoice_")
 */
class InvoiceController extends AbstractController
{
    /** @var InvoiceService */
    private $invoiceService;

    /** @var LoggerInterface */
    private $loggerService;

    /**
     * @param InvoiceService  $invoiceService
     * @param LoggerInterface $loggerService
     */
    public function __construct(
        InvoiceService $invoiceService,
        LoggerInterface $loggerService
    ) {
        $this->invoiceService = $invoiceService;
        $this->loggerService = $loggerService;
    }

    /**
     * @Route("/invoice", name="show", methods={"GET"})
     *
     * @return Response
     *
     * @throws Exception
     */
    public function show(): Response
    {
        try {
            $invoices = $this->invoiceService->getInvoices();
        } catch (Throwable $error) {
            $invoices = [];
            $this->loggerService->error($error->getMessage());
        }

        return $this->response(['invoices' => $invoices]);
    }

    /**
     * @param Request $request
     *
     * @Route("/invoice", name="create", methods={"POST"})
     *
     * @return Response
     *
     * @throws Exception
     */
    public function create(Request $request): Response
    {
        try {
            $this->invoiceService->createInvoices($request->files->get('csv'));
        } catch (Throwable $error) {
            $this->loggerService->error($error->getMessage());
        }

        return $this->response();
    }

    /**
     * @param array $data
     *
     * @return Response
     */
    private function response(array $data = []): Response
    {
        $serializer = $this->container->get('serializer');

        return new Response($serializer->serialize($data, 'json'));
    }
}
